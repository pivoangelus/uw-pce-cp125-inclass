//
//  AppDelegate.h
//  helloAlertController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

