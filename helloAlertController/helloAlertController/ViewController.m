//
//  ViewController.m
//  helloAlertController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
- (IBAction)greetingButtonTapped:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)greetingButtonTapped:(id)sender;
{
    NSString *newGreeting = [NSString stringWithFormat:@"Hello, %@!", self.nameTextField.text];
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Hello"
                                                                message:newGreeting
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    
//    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Hello"
//                                                                message:newGreeting
//                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:ac animated:YES completion:nil];
}
@end
