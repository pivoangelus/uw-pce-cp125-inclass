//
//  ViewController.m
//  helloAutoLayout
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *greetingLabel;
@property (weak, nonatomic) IBOutlet UITextField *helloTextField;
@property (weak, nonatomic) IBOutlet UIButton *greetingButton;

@property (nonatomic, strong) NSMutableArray *constraints;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)updateViewConstraints;
{
    [super updateViewConstraints];
    
    if (self.constraints == nil)
    {
        NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.greetingLabel
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1.0
                                                                    constant:0.0];
        
        NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.greetingLabel
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.view
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1.0
                                                                    constant:0.0];
        self.constraints = [[NSMutableArray array] init];
        [self.constraints addObject:centerX];
        [self.constraints addObject:centerY];
        
        [NSLayoutConstraint activateConstraints:self.constraints];
        //centerX.active = YES;
        //centerY.active = YES;
        
        //[NSLayoutConstraint deactivateConstraints:self.constraints];
    }
}

@end
