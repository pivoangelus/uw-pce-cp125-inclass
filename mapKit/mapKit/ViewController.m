//
//  ViewController.m
//  mapKit
//
//  Created by Justin Andros on 2/28/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ViewController () <CLLocationManagerDelegate, MKMapViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) MKLocalSearch *localSearch;

@end

@implementation ViewController

- (void)viewDidLoad;
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated;
{
    [super viewDidAppear:animated];
    
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
//        [self.locationManager startUpdatingLocation];
        self.mapView.showsUserLocation = YES;
    }
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation;
{
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 10, 10) animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation;
{
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        MKPinAnnotationView *greenPin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"GreenPinView"];
        if (greenPin == nil)
        {
            greenPin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"GreenPinView"];
            greenPin.pinColor = MKPinAnnotationColorGreen;
            greenPin.animatesDrop = YES;
            greenPin.canShowCallout = YES;
            
            // create a button that has (i) inside -- used to give additional info
            UIButton *calloutButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [calloutButton  addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            
            greenPin.rightCalloutAccessoryView = calloutButton;
        }
        else
        {
            greenPin.annotation =annotation;
        }
        return greenPin;
    }
    
    return nil; // default is blue dot or, if a pin, red
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
{
    NSLog (@"%@", view.annotation.title);
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    searchRequest.region = self.mapView.region;
    searchRequest.naturalLanguageQuery = searchBar.text;
    
    if (self.localSearch.isSearching)
    {
        [self.localSearch cancel];
        self.localSearch = nil;
    }
    
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        [self.mapView removeAnnotations:self.mapView.annotations];  // user location remains, only requests are removed from the map
        
        for (MKMapItem *mapItem in response.mapItems)
        {
            [self.mapView addAnnotation:mapItem.placemark];
        }
    }];
}

@end
