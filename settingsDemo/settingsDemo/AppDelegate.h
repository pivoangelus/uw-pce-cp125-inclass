//
//  AppDelegate.h
//  settingsDemo
//
//  Created by Justin Andros on 3/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const EmailAdressSettingsKey;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

