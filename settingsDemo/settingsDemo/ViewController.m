//
//  ViewController.m
//  settingsDemo
//
//  Created by Justin Andros on 3/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // these can be written to disk
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // can be commented out if already run
    [defaults setObject:@"jandros@gmail.com" forKey:EmailAdressSettingsKey];
    [defaults synchronize]; // want to make sure we do this as much as possible, low maintenance
    
    NSString * email = [[NSUserDefaults standardUserDefaults] stringForKey:EmailAdressSettingsKey];
    
    NSLog(@"email: %@", email);
}

@end
