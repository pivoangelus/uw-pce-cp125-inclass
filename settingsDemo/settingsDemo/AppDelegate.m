//
//  AppDelegate.m
//  settingsDemo
//
//  Created by Justin Andros on 3/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

// should be defined in a header somewhere for others
NSString * const EmailAdressSettingsKey = @"EmailAdressSettingsKey";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // defaults if nothing exists
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{ EmailAdressSettingsKey : @"jandros@uw.edu" }];
    return YES;
}

@end
