//
//  NameInputViewController.h
//  helloDelegateWorld
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

// forward declare.. its coming!
@protocol NameInputViewControllerDelegate;

@interface NameInputViewController : UIViewController

@property (nonatomic, weak) id<NameInputViewControllerDelegate> delegate;

@end

@protocol NameInputViewControllerDelegate <NSObject>

- (void)nameInputViewController:(NameInputViewController *)nameInputViewController didEnterName:(NSString *)name;

@optional
- (void)nameInputViewControllerDidCancel:(NameInputViewController *)nameInputViewController;

@end
