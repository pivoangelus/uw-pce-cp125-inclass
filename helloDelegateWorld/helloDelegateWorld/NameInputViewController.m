//
//  NameInputViewController.m
//  helloDelegateWorld
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "NameInputViewController.h"

@interface NameInputViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

- (IBAction)doneButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end

@implementation NameInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)doneButtonTapped:(id)sender;
{
    [self.delegate nameInputViewController:self didEnterName:self.nameTextField.text];
}

- (IBAction)cancelButtonTapped:(id)sender;
{
    if ([self.delegate respondsToSelector:@selector(nameInputViewControllerDidCancel:)])
    {
        [self.delegate nameInputViewControllerDidCancel:self];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
