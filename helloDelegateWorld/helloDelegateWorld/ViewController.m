//
//  ViewController.m
//  helloDelegateWorld
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"
#import "NameInputViewController.h"

@interface ViewController () <NameInputViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
{
    // test nav controller is there
    NSAssert([segue.destinationViewController isKindOfClass:[UINavigationController class]], @"destination controller expected to be of type UINavigationController");
    
    UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
    
    // get the top view of the nav.. that may be the view we want.. otherwise look for it :p
    NSAssert([navController.topViewController isKindOfClass:[NameInputViewController class]], @"topViewController expected to be of type NameInputViewController");
    
    // set the view as being a delegate
    NameInputViewController *nameInputViewController = (NameInputViewController *)navController.topViewController;
    nameInputViewController.delegate = self;
}

#pragma mark - NameInputViewControllerDelegate
//- (void)nameInputViewControllerDidCancel:(NameInputViewController *)nameInputViewController;
//{
//}

- (void)nameInputViewController:(NameInputViewController *)nameInputViewController didEnterName:(NSString *)name;
{
    self.messageLabel.text = [NSString stringWithFormat:@"Hello, %@", name];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
