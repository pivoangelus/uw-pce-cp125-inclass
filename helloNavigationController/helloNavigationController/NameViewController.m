//
//  NameViewController.m
//  helloNavigationController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "NameViewController.h"
#import "GreetingViewController.h"

@interface NameViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
- (IBAction)pushButtonPressed:(id)sender;

@end

@implementation NameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Name";
}

- (IBAction)pushButtonPressed:(id)sender;
{
    NSString *newGreeting = [NSString stringWithFormat:@"Hello, %@!", self.nameTextField.text];
    
    GreetingViewController *greetingVC = [[GreetingViewController alloc] init];
    greetingVC.greeting = newGreeting;
    
    [self.navigationController pushViewController:greetingVC animated:YES];
}
@end
