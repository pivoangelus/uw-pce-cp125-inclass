//
//  GreetingViewController.m
//  helloNavigationController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "GreetingViewController.h"

@interface GreetingViewController ()
@property (weak, nonatomic) IBOutlet UILabel *greetingLabel;

@end

@implementation GreetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Greeting";

    self.greetingLabel.text = self.greeting;
}



@end
