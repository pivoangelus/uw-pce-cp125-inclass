//
//  ViewController.m
//  helloWorld
//
//  Created by Justin Andros on 1/12/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)button:(id)sender
//{
//    NSString *newGreeting = [NSString stringWithFormat:@"Hello %@!", self.textfield.text];
//    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Hello"
//                                                                             message:newGreeting
//                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
//    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//    
//    [self presentViewController:alertController animated:YES completion:nil];
//}

@end
