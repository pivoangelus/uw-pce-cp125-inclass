//
//  AppDelegate.m
//  locationDemo
//
//  Created by Justin Andros on 2/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

// add NSLocationWhenInUseUsageDescription to plist
// when running need to change location to get a location (simulator bug)
// (1) at prompt: po locations -- shows locations
//                po [[locations firstObject] class] -- tells you the first object's class. helpful for determining what you can do with the object.
// (2) at prompt: po placemarks -- show marker details

#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManger;
@property (strong, nonatomic) CLGeocoder *geocoder; // async process

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    self.locationManger = [[CLLocationManager alloc] init];
    self.locationManger.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManger.delegate = self;
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        // requestWhenInUseAuthorization is the key in the plist. we can only respond to those found in the plist.
        [self.locationManger requestWhenInUseAuthorization];
    }
    
    return YES;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.locationManger startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
{
    [self.locationManger stopUpdatingLocation];
    
    CLLocation *location = [locations firstObject];
    self.geocoder = [[CLGeocoder alloc] init]; // async process, need a location pointer to this
    
    [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"placemarks: %@", placemarks); // (2) -- break
    }];
    
    //NSLog(@"location: %@",locations); // (1) -- break
}

@end
