//
//  AppDelegate.m
//  helloTabBarController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"
#import "NameViewController.h"
#import "GreetingViewController.h"

@interface AppDelegate () <UITabBarControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.delegate = self;
    
    NameViewController *nameViewController = [[NameViewController alloc] init];
    GreetingViewController *greetingViewController = [[GreetingViewController alloc] init];
    
    tabBarController.viewControllers = @[nameViewController, greetingViewController];
    
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController;
{
    NameViewController *nameVC = (NameViewController *) tabBarController.viewControllers[0];
    GreetingViewController *greetingVC = (GreetingViewController *) tabBarController.viewControllers[1];
    
    NSString *newGreeting = [NSString stringWithFormat:@"Hello, %@!", nameVC.name];
    greetingVC.greeting = newGreeting;
    
    return YES;
}

@end
