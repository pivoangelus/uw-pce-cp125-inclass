//
//  NameViewController.m
//  helloTabBarController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "NameViewController.h"

@interface NameViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@end

@implementation NameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBarItem.image = [UIImage imageNamed:@"colors"];
    self.tabBarItem.title = @"Name";
    
    self.nameTextField.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return YES;
}

- (NSString *)name;
{
    return self.nameTextField.text;
}

@end
