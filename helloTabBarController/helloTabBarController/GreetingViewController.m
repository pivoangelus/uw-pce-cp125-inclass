//
//  GreetingViewController.m
//  helloTabBarController
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "GreetingViewController.h"

@interface GreetingViewController ()
@property (weak, nonatomic) IBOutlet UILabel *greetingLabel;

@end

@implementation GreetingViewController

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.tabBarItem.image = [UIImage imageNamed:@"data"];
        self.tabBarItem.title = @"Greeting";
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated;
{
    self.greetingLabel.text = self.greeting;
}

@end
