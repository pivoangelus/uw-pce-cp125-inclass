//
//  ViewController.m
//  UWLocalizationDemo
//
//  Created by Jake Carter on 3/8/15.
//  Copyright (c) 2015 Jake Carter. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *helloCodeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.helloCodeLabel.text = NSLocalizedString(@"view-controller.hello-code-label-text", @"A greeting to the user"); // @"Hello, world!";
    self.imageView.image = [UIImage imageNamed:@"map"];
}

@end
