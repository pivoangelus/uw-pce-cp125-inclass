//
//  AppDelegate.h
//  UWLocalizationDemo
//
//  Created by Jake Carter on 3/8/15.
//  Copyright (c) 2015 Jake Carter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

