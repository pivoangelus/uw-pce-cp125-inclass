//
//  TableViewController.m
//  tableViewDemo
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "TableViewController.h"
#import "CustomTableViewCell.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCellid"];
//    
//    cell.textLabel.text = [NSString stringWithFormat:@"Item %ld", (long)indexPath.row];
//    
//    return cell;
    
    CustomTableViewCell *customCell = [tableView dequeueReusableCellWithIdentifier:@"CustomCellid"];
    
    customCell.nameLabel.text = [NSString stringWithFormat:@"Name: %ld", (long)indexPath.row];
    customCell.ageLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    
    return customCell;
}

// height set -- need both methods to prevent warnings. -_-
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}

@end
